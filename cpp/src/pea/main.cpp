#include <unordered_map>
#include <sys/time.h>
#include <iostream>
#include <fstream>
#include <memory>
#include <chrono>
#include <string>

#include "omp.h"

using namespace std::literals::chrono_literals;
using std::chrono::system_clock;
using std::chrono::time_point;
using std::string;

#include <boost/log/utility/setup/console.hpp>
#include <boost/log/trivial.hpp>
#include <boost/filesystem.hpp>


#include "minimumConstraints.hpp"
#include "networkEstimator.hpp"
#include "peaCommitVersion.h"
#include "algebraTrace.hpp"
#include "rtsSmoothing.hpp"
#include "corrections.hpp"
#include "streamTrace.hpp"
#include "writeClock.hpp"
#include "acsConfig.hpp"
#include "acsStream.hpp"
#include "testUtils.hpp"
#include "ionoModel.hpp"
#include "station.hpp"
#include "summary.hpp"
#include "preceph.hpp"
#include "satStat.hpp"
#include "common.hpp"
#include "newsnx.hpp"
#include "orbits.hpp"
#include "gaTime.hpp"
#include "antenna.h"
#include "debug.hpp"
#include "plot.hpp"
#include "enums.h"
#include "ppp.hpp"
#include "snx.hpp"
#include "trop.h"
#include "vmf3.h"


nav_t	nav		= {};
int		epoch	= 1;

void removeUnprocessed(
	ObsList&	obsList)
{
	for (auto& obs : obsList)
	{
		if (acsConfig.process_sys[obs.Sat.sys] == false)
		{
			obs.excludeSystem = true;
		}
	}
}

void mainOncePerEpochPerStation(
	GTime&		tsync,
	Station&	rec,
	double*		orog,
	gptgrid_t&	gptg)
{
	TestStack ts(rec.id);

	Trace& trace = *rec.trace.get();

	trace << std::endl << "################# Starting Epoch " << epoch << " << ############" << std::endl;

	BOOST_LOG_TRIVIAL(debug)
	<< "\tRead " << rec.obsList.size()
	<< " observations for station " << rec.id;

	removeUnprocessed(rec.obsList);

	sinexPerEpochPerStation(tsync, rec);

	/* linear combinations */
	for (auto& obs : rec.obsList)
	{
		obs.satStat_ptr->lc_pre = obs.satStat_ptr->lc_new;
		obs.satStat_ptr->lc_new = {};
	}
	obs2lcs		(trace,	rec.obsList);

	/* cycle slip/clock jump detection and repair */
	detectslips	(trace,	rec.obsList);
	detectjump	(trace,	rec.obsList, acsConfig.elevation_mask, rec.cj);

	for (auto& obs			: rec.obsList)
	for (auto& [ft, Sig]	: obs.Sigs)
	{
		if (obs.satStat_ptr->sigStatMap[ft].slip.any)
		{
			rec.slipCount++;
			break;
		}
	}

	gtime_t prevTime = rec.rtk.sol.time;

	//do a spp on the observations
	sppos(trace, rec.obsList, rec.rtk.sol);
	if (prevTime.time != 0)
	{
		rec.rtk.tt = timediff(rec.rtk.sol.time, prevTime);
	}

	if (acsConfig.process_ionosphere)
	{
		update_receivr_measr(rec);
	}

	/* exclude measurements of eclipsing satellite (block IIA) */
	if (acsConfig.reject_eclipse)
	{
		testeclipse(rec.obsList);
	}

	if (acsConfig.process_user)
	{
		pppos(trace, rec.rtk, rec.obsList, rec.station);		//todo aaron, added station, might be wrong.
	}

	if (acsConfig.process_network)
	{
		// if the vmf3 option has been selected, use it, otherwise send null
		vmf3_t*	rtkVmf3_ptr = nullptr;
		double*	rtkOrog_ptr = nullptr;
		if	(!acsConfig.tropOpts.vmf3dir.empty())
		{
			rtkVmf3_ptr = &rec.vmf3;
			rtkOrog_ptr = orog;
		}

		/* use snx coordinates */
		rec.rtk.sol.sppRRec = rec.snx.pos;		//todo aaron, dont do this

		/* observed minus computed for each satellites */
		pppomc(trace, rec.rtk, rec.obsList, gptg, rec.cj, rec.station, rtkVmf3_ptr, rtkOrog_ptr);
	}

	if	( (acsConfig.process_rts)
		&&(acsConfig.pppOpts.rts_lag > 0)
		&&(epoch > acsConfig.pppOpts.rts_lag))
	{
		KFState rts = RTS_Process(rec.rtk.pppState);
		std::ofstream rtsTrace(rec.rtk.pppState.rts_smoothed_filename, std::ofstream::out | std::ofstream::app);
		pppoutstat(rtsTrace, rts);
	}
}

void mainOncePerEpoch(
	GTime&			tsync,
	KFState&		ionKFState,
	KFState&		netKFState,
	Trace&			ionTrace,
	Trace&			netTrace,
	StationList&	epochStations,
	double			tgap)
{
	TestStack ts("1/Ep");
	double ep[6] = {};
	time2epoch(tsync, ep);		//todo aaron, change to string function too

	if (acsConfig.process_ionosphere)
	{
		ionKFState.initFilterEpoch();
		update_ionosph_model(std::cout, epochStations, ionKFState, tgap);
		ionex_file_write(ionTrace, tsync, ionKFState);
		iono_write_DSB	(ionTrace, tsync, ionKFState);
		ionKFState.outputStates(ionTrace);
	}

	if (acsConfig.process_network)
	{
		netTrace << "\r\n------=============== " << epoch << " =============-----------\r\n";

		int info = networkEstimator(netTrace, epochStations, netKFState, tgap);

		netKFState.outputStates(netTrace);

		// Output clock product body part
		if	( acsConfig.output_clocks
			&&info == false)
		{
			if (epoch == 1)
			{
				outputClockfileHeader(netKFState, ep);
			}
			outputReceiverClocks(netKFState, ep);
			outputSatelliteClocks(netKFState, ep);
		}

		if	( (acsConfig.process_rts)
			&&(acsConfig.netwOpts.rts_lag > 0)
			&&(epoch > acsConfig.netwOpts.rts_lag))
		{
			KFState rts = RTS_Process(netKFState);
			std::ofstream rtsTrace(netKFState.rts_smoothed_filename, std::ofstream::out | std::ofstream::app);
			if (rts.time != GTime::noTime())
			{
				pppoutstat(rtsTrace, rts);
			}
		}
	}

	if (acsConfig.output_persistance)
	{
		outputPersistantNav(acsConfig.persistance_filename);
	}

	TestStack::testStatus();
}

void mainPostProcessing(
	GTime&					tsync,
	KFState&				ionKFState,
	KFState&				netKFState,
	Trace&					ionTrace,
	Trace&					netTrace,
	map<string, Station>&	stationMap)
{
	if (acsConfig.process_ionosphere)
	{
		ionex_file_write(ionTrace, tsync, ionKFState, true);
		iono_write_DSB	(ionTrace, tsync, ionKFState, true);
	}

	if	( acsConfig.process_network
		&&acsConfig.process_minimum_constraints)
	{
		BOOST_LOG_TRIVIAL(info)
		<< std::endl
		<< "---------------PROCESSING NETWORK WITH MINIMUM CONSTRAINTS ------------- " << std::endl;

		minimum(netTrace, netKFState);
		netKFState.outputStates(netTrace);
	}

	if (acsConfig.output_sinex)
	{
		sinexPostProcessing(tsync, stationMap, netKFState);
	}

	if	(acsConfig.process_rts)
	{
		if (acsConfig.pppOpts.rts_lag < 0)
		for (auto& [id, rec] : stationMap)
		{
			BOOST_LOG_TRIVIAL(info)
			<< std::endl
			<< "---------------PROCESSING PPP WITH RTS------------------------- " << std::endl;

			RTS_Process(rec.rtk.pppState, true);

			BOOST_LOG_TRIVIAL(info)
			<< std::endl
			<< "---------------OUTPUTTING PPP RTS RESULTS---------------------- " << std::endl;

			RTS_Output(rec.rtk.pppState);
		}

		if (acsConfig.netwOpts.rts_lag < 0)
		{
			BOOST_LOG_TRIVIAL(info)
			<< std::endl
			<< "---------------PROCESSING NETWORK WITH RTS--------------------- " << std::endl;

			RTS_Process(netKFState, true);

			BOOST_LOG_TRIVIAL(info)
			<< std::endl
			<< "---------------OUTPUTTING NETWORK RTS RESULTS------------------ " << std::endl;

			RTS_Output(netKFState);
		}

		if (acsConfig.ionFilterOpts.rts_lag < 0)
		{
			BOOST_LOG_TRIVIAL(info)
			<< std::endl
			<< "---------------PROCESSING IONOSPHERE WITH RTS------------------ " << std::endl;

			RTS_Process(ionKFState, true);

			BOOST_LOG_TRIVIAL(info)
			<< std::endl
			<< "---------------OUTPUTTING IONOSPHERE RTS RESULTS--------------- " << std::endl;

			RTS_Output(ionKFState);
		}
	}

	if (acsConfig.output_plots)
	{
		BOOST_LOG_TRIVIAL(info)
		<< std::endl
		<< "Plotting..." << std::endl;

		outputPlots();
	}

	TestStack::printStatus(true);

    if (acsConfig.output_summary)
	{
		//todo aaron # what processing options we enabled
		outputSummaries(stationMap);
	}

	if (acsConfig.process_network) // && acsConfig.satOpts.orb)
	{
		outputOrbit(netKFState);
	}
}

int main(int argc, char **argv)
{
    tracelevel(5); // TODO why do we have a hardcoded value here ?? MM 

    boost::log::core::get()->set_filter (boost::log::trivial::severity >= boost::log::trivial::info);
	boost::log::add_console_log(std::cout, boost::log::keywords::format = "%Message%");

    BOOST_LOG_TRIVIAL(info)
	<< "PEA starting... (v" PEA_COMMIT_VERSION ")" << std::endl;

	auto peaStartTime = boost::posix_time::from_time_t(system_clock::to_time_t(system_clock::now()));

    if (!configure(argc, argv))
	{
        BOOST_LOG_TRIVIAL(error) 	<< "Incorrect configuration";
        BOOST_LOG_TRIVIAL(info) 	<< "PEA finished";

        return EXIT_FAILURE;
    }

	doDebugs();

    TestStack::openData();

	BOOST_LOG_TRIVIAL(info)
    << "Threading with " << Eigen::nbThreads()
	<< " threads for Eigen" << std::endl;

    // Ensure the output directories exist
	if (!acsConfig.log_directory		.empty())	boost::filesystem::create_directories(acsConfig.log_directory);
    if (!acsConfig.residuals_directory	.empty())	boost::filesystem::create_directories(acsConfig.residuals_directory);
    if (!acsConfig.summary_directory	.empty())	boost::filesystem::create_directories(acsConfig.summary_directory);
    if (!acsConfig.trace_directory		.empty())	boost::filesystem::create_directories(acsConfig.trace_directory);
	if (!acsConfig.clocks_directory		.empty())	boost::filesystem::create_directories(acsConfig.clocks_directory);
	if (!acsConfig.ionex_directory		.empty())	boost::filesystem::create_directories(acsConfig.ionex_directory);
	if (!acsConfig.iontrace_directory	.empty())	boost::filesystem::create_directories(acsConfig.iontrace_directory);
	if (!acsConfig.plot_directory		.empty())	boost::filesystem::create_directories(acsConfig.plot_directory);
	if (!acsConfig.sinex_directory		.empty())	boost::filesystem::create_directories(acsConfig.sinex_directory);
	if (!acsConfig.testOpts.directory	.empty())	boost::filesystem::create_directories(acsConfig.testOpts.directory);

    BOOST_LOG_TRIVIAL(info)
	<< "Logging with trace level:" << acsConfig.trace_level << std::endl;

    tracelevel(acsConfig.trace_level);


    double		orog[NGRID] = {};	 		/* vmf3 orography information, config->orography */
    gptgrid_t	gptg		= {};         	/* gpt grid information */

    for (auto& atxfile : acsConfig.atxfiles)
	{
		BOOST_LOG_TRIVIAL(debug)
		<< "Loading receiver PCV from file " << atxfile;

		if (!readantexf(atxfile.c_str(), nav.pcvList))
		{
			BOOST_LOG_TRIVIAL(error)
			<< "Unable to load receiver PCV from file " << atxfile;

			return EXIT_FAILURE;
		}
	}

    //read orography file for VMF3
    {
		int orog_sucess = false;
		if 	( acsConfig.tropOpts.orography	.empty() == false
			&&acsConfig.tropOpts.vmf3dir	.empty() == false)
		{
			orog_sucess = readorog(acsConfig.tropOpts.orography.c_str(), orog);
		}

		if (orog_sucess == false)
		{
			// read gpt2 grid file
			readgrid(acsConfig.tropOpts.gpt2grid.c_str(), &gptg);
		}
	}

    for (auto& erpfile : acsConfig.erpfiles)
	{
		if (!erpfile.empty())
		{
			BOOST_LOG_TRIVIAL(debug)
			<< "Loading ERP from ERP file " << erpfile;

			readerp(erpfile.c_str(), &nav.erp);
		}
	}

	if (acsConfig.output_persistance)
	{
		inputPersistantNav(acsConfig.persistance_filename);
	}

    if (acsConfig.orbfiles.size() > 0)
	{
		/* orbit info from orbit file */
		for (auto& orbfile : acsConfig.orbfiles)
		{
			BOOST_LOG_TRIVIAL(debug)
			<< "Reading in the orbit file" << orbfile << std::endl;

			readorbit(orbfile.c_str(), nav.orbpod);
		}
		orb2sp3(nav);
    }
    else
	{
		for (auto& sp3file : acsConfig.sp3files)
		{
			BOOST_LOG_TRIVIAL(debug)
			<< "Loading SP3 file " << sp3file << std::endl;

			readsp3(sp3file.c_str(), &nav, 0);
		}
    }

    for (auto& navfile : acsConfig.navfiles)
	{
		BOOST_LOG_TRIVIAL(debug)
		<< "Loading navigation file " << navfile;

		char	type_nav 				= {};
		double	version_nav 			= {};
		E_Sys	navigation_system_nav	= {};
		int		time_system_nav 		= {};

		FILE *fpnav = fopen(navfile.c_str(), "r");

		if (!fpnav)
		{
			BOOST_LOG_TRIVIAL(error)
			<< "Unable to open navigation file " << navfile;

			return EXIT_FAILURE;
		}

		// Reads twice, the header first time called, then the data
		for (string a : {"header of navigation file", "navigation data"})
		{
			if  ( !feof(fpnav)
				&&!readrnxfppde(fpnav, {}, {}, 0, nullptr, 0, 1, &type_nav, nullptr, &nav, nullptr, &version_nav, &navigation_system_nav, &time_system_nav, nullptr))
			{
				BOOST_LOG_TRIVIAL(error)
				<< "ERROR: Failed to read " << a;

				return EXIT_FAILURE;
			}
		}

		fclose(fpnav);
	}

    // Read the CLK files
    for (auto& clkfile : acsConfig.clkfiles)
	{
		/* CLK file - RINEX 3 */
		BOOST_LOG_TRIVIAL(debug)
		<< "Loading CLK from CLK file " << clkfile;

		char	type 		= {};
		double	version 	= {};
		E_Sys	nav_system	= {};
		int		time_system	= {};

		FILE *fpnav = fopen(clkfile.c_str(), "r");

		if (!fpnav)
		{
			BOOST_LOG_TRIVIAL(error)
			<< "Unable to open CLK file " << clkfile;

			return EXIT_FAILURE;
		}

		// Reads twice, the header first time called, then the data
		for (string a : {"header of CLK file", "CLK data"})
		{
			if  ( !feof(fpnav)
				&&!readrnxfppde(fpnav, {}, {}, 0, nullptr, 1, 1, &type, nullptr, &nav, nullptr, &version, &nav_system, &time_system, nullptr))
			{
				BOOST_LOG_TRIVIAL(error)
				<< "ERROR: Failed to read " << a;

				return EXIT_FAILURE;
			}
		}

		fclose(fpnav);
	}

	if (acsConfig.process_ionosphere)
	{
		if	(!config_ionosph_model())
		{
			BOOST_LOG_TRIVIAL(error)
			<< "Error in Ionosphere Model configuration";

			return EXIT_FAILURE;
		}
	}

    // Initialise each stream...
    BOOST_LOG_TRIVIAL(info)
	<< "Initialising stations...";

	map<string, Station> stationMap;

    for (auto& [id, s] : acsConfig.obsStreamMultimap)
    {
		BOOST_LOG_TRIVIAL(debug)
		<< id;

		auto& rec = stationMap[id];

		rec.id = id;

		auto& station = rec.station;
		station = s->station;

		strcpy(station.name, id.c_str());
	}

    // Read the DCB file
    for (auto& dcbfile : acsConfig.dcbfiles)
	{
        if (dcbfile.find(".DCB") != -1)
		{
			/* DCB file - RINEX 2 */
            BOOST_LOG_TRIVIAL(debug)
			<< "Loading DCB from DCB file " << dcbfile.c_str();

			//get list of stations for dcb use.
			list<sta_t*> stations;
			for (auto& [id, rec] : stationMap)
			{
				stations.push_back(&rec.station);
			}
 			readdcb(dcbfile.c_str(), &nav, stations);   /* only for satellite */
        }
        else if (dcbfile.find(".BSX") != -1)
		{
			/* BSX file - RINEX 3 */
            BOOST_LOG_TRIVIAL(debug)
			<< "Loading DCB from BSX file " << dcbfile.c_str();

            readdcb3(dcbfile.c_str(), &nav.mbias);
            nav.optbias = 1;
        }
    }

    for (auto& ionfile : acsConfig.ionfiles)
	{
		BOOST_LOG_TRIVIAL(debug)
		<< "Loading Ionosphere from file " << ionfile.c_str();

		readtec(ionfile.c_str(), &nav, 0, nullptr);
	}

    for (auto& ionfile : acsConfig.ionfiles)
	{
		BOOST_LOG_TRIVIAL(debug)
		<< "Loading Ionosphere from file " << ionfile.c_str();

		readtec(ionfile.c_str(), &nav, 0, nullptr);
	}

	for (auto& snxfile : acsConfig.snxfiles)
	{
		BOOST_LOG_TRIVIAL(debug)
		<< "Loading SINEX file " <<  snxfile << std::endl;

		bool fail = read_sinex(snxfile);
		if (fail)
		{
			BOOST_LOG_TRIVIAL(error)
			<< "Unable to load SINEX file " << snxfile;

			return EXIT_FAILURE;
		}
	}

	for (auto& [id, rec] : stationMap)
	{
        BOOST_LOG_TRIVIAL(debug)
		<< "Initialising station " << id;

		if (acsConfig.output_trace)
		{
			string path_trace = acsConfig.trace_filename;
			replaceString(path_trace, "<STATION>", id);

			// Create the trace file
			BOOST_LOG_TRIVIAL(debug)
			<< "\tCreating trace file for stream " << id;

			rec.trace = std::make_unique<std::ofstream>(path_trace);
			if (rec.trace.get() == nullptr)
			{
				BOOST_LOG_TRIVIAL(error)
				<< "Could not create trace file for " << id << " at " << path_trace;
			}
		}
		else
		{
			rec.trace = std::make_unique<std::ofstream>();
		}

		rec.rtk.opt.anttype = rec.station.antdes;

        // Read the BLQ file
        bool found = false;
		for (auto& blqfile : acsConfig.blqfiles)
		{
			found = readblq(blqfile.c_str(), id.c_str(), rec.rtk.opt.odisp[0]);

			if (found)
			{
				break;
			}
		}

		if (found == false)
		{
            BOOST_LOG_TRIVIAL(warning)
			<< "No BLQ for " << id;
        }

		if (acsConfig.process_user)
		{
			rec.rtk.pppState.max_filter_iter	= acsConfig.pppOpts.max_filter_iter;
			rec.rtk.pppState.max_prefit_remv	= acsConfig.pppOpts.max_prefit_remv;
			rec.rtk.pppState.inverter			= acsConfig.pppOpts.inverter;
			rec.rtk.pppState.output_residuals	= acsConfig.output_residuals;

			rec.rtk.pppState.rejectCallbacks.push_back(countSignalErrors);
			rec.rtk.pppState.rejectCallbacks.push_back(deweightMeas);
		}
	}

	BOOST_LOG_TRIVIAL(debug)
	<< "\tCreating trace files ";

	std::ofstream netTrace;
	if (acsConfig.output_summary)
	{
		netTrace.open(acsConfig.summary_filename);
	}

	if (acsConfig.process_network)
	if (acsConfig.output_clocks)
	{
		//just create a new empty file, it will be reopened later
		std::ofstream(acsConfig.clocks_filename);
	}

	std::ofstream ionTrace;
	if (acsConfig.process_ionosphere)
	if (acsConfig.output_iontrace)
	{
		ionTrace.open(acsConfig.iontrace_filename);
	}
	if (acsConfig.process_ionosphere)
	if (acsConfig.output_ionex)
	{
		//just create a new empty file, it will be reopened later
		std::ofstream(acsConfig.ionex_filename);
		std::ofstream(acsConfig.iondsb_filename);
	}

	KFState netKFState	= {};
	{
		netKFState.max_filter_iter	= acsConfig.netwOpts.max_filter_iter;
		netKFState.max_prefit_remv	= acsConfig.netwOpts.max_prefit_remv;
		netKFState.inverter			= acsConfig.netwOpts.inverter;
		netKFState.output_residuals	= acsConfig.output_residuals;
		netKFState.rejectCallbacks.push_back(deweightMeas);
	}

	KFState ionKFState	= {};
	{
		ionKFState.max_filter_iter	= acsConfig.ionFilterOpts.max_filter_iter;
		ionKFState.max_prefit_remv	= acsConfig.ionFilterOpts.max_prefit_remv;
		ionKFState.inverter			= acsConfig.ionFilterOpts.inverter;
		netKFState.output_residuals	= acsConfig.output_residuals;
		netKFState.rejectCallbacks.push_back(deweightMeas);
	}

	if (acsConfig.process_rts)
	{
		if (acsConfig.pppOpts.rts_lag)
		for(auto& [id, rec] : stationMap)
		{
			string rts_filename		= acsConfig.pppOpts.rts_filename;

			replaceString(rts_filename, "<STATION>", id);

			initFilterTrace(rec.rtk.pppState, rts_filename, acsConfig.pppOpts.rts_lag);
		}

		if (acsConfig.netwOpts.rts_lag)
		{
			initFilterTrace(netKFState, acsConfig.netwOpts.rts_filename, acsConfig.netwOpts.rts_lag);
		}

		if (acsConfig.ionFilterOpts.rts_lag)
		{
			initFilterTrace(ionKFState, acsConfig.ionFilterOpts.rts_filename, acsConfig.ionFilterOpts.rts_lag);
		}
	}

    gtime_t tsync		= GTime::noTime();
	if (acsConfig.start_epoch.is_not_a_date_time() == false)
	{
		tsync.time = boost::posix_time::to_time_t(acsConfig.start_epoch);
	}

    double tgap = acsConfig.epoch_interval;

    BOOST_LOG_TRIVIAL(info)
	<< std::endl;
    BOOST_LOG_TRIVIAL(info)
	<< "Starting to process epochs...";

	TestStack ts(acsConfig.config_description);

    //============================================================================
    // MAIN PROCESSING LOOP														//
    //============================================================================

    // Read the observations for each station and do stuff
	bool complete = false;
    while (complete == false)
    {
		BOOST_LOG_TRIVIAL(info)
		<< std::endl;
		BOOST_LOG_TRIVIAL(info)
		<< "Starting epoch #" << epoch;

		TestStack ts("Epoch " + std::to_string(epoch));

		StationList epochStations;

		bool repeat		= true;
		auto maxDelay	= system_clock::now() + std::chrono::milliseconds((int)(acsConfig.wait_next_epoch * 1000));
		auto breakTime	= maxDelay;

		//get observations from streams (allow some delay between stations, and retry, to ensure all messages for the epoch have arrived)
		while	( repeat
				&&system_clock::now() < breakTime)
		{
			repeat = false;

			//remove any dead streams
			for (auto iter = acsConfig.obsStreamMultimap.begin(); iter != acsConfig.obsStreamMultimap.end(); )
			{
				ObsStream& obsStream = *iter->second;

				if (obsStream.isDead())
				{
					iter = acsConfig.obsStreamMultimap.erase(iter);
				}
				else
				{
					iter++;
				}
			}

			if (acsConfig.obsStreamMultimap.empty())
			{
				BOOST_LOG_TRIVIAL(info)
				<< std::endl;
				BOOST_LOG_TRIVIAL(info)
				<< "Inputs finished at epoch #" << epoch;

				complete = true;

				break;
			}

			for (auto& [id, s] : acsConfig.obsStreamMultimap)
			{
				ObsStream&	obsStream	= *s;
				auto&		rec			= stationMap[id];

				if	( (rec.obsList.size() > 0)
					&&(rec.obsList.front().time == tsync))
				{
					//already have observations for this epoch.
					continue;
				}

				//try to get some data
				rec.obsList = obsStream.getObs(tsync);

				if (rec.obsList.empty())
				{
					//failed to get observations, try again later
					repeat = true;

					continue;
				}

				if (breakTime == maxDelay)
				{
					//first observation found for this epoch, give any other stations some time to get their observations too
					breakTime = system_clock::now() + std::chrono::milliseconds((int)(acsConfig.wait_all_stations * 1000));
				}

				//calculate statistics
				{
					if (rec.firstEpoch	== GTime::noTime())		{	rec.firstEpoch	= rec.obsList.front().time;		}
					if (tsync			== GTime::noTime())		{	tsync			= rec.obsList.front().time;		}
																	rec.lastEpoch	= rec.obsList.front().time;
					rec.epochCount++;
					rec.obsCount += rec.obsList.size();

					for (auto& obs				: rec.obsList)
					for (auto& [ft, sigList]	: obs.SigsLists)
					for (auto& sig				: sigList)
					{
						rec.codeCount[sig.code]++;
					}

					for (auto& obs				: rec.obsList)
					{
						rec.satCount[obs.Sat.id()]++;
					}
				}

				//prepare and connect navigation objects to the observations
				for (auto& obs : rec.obsList)
				{
					obs.satNav_ptr					= &nav.satNavMap[obs.Sat];
					obs.satNav_ptr->eph_ptr 		= seleph	(tsync, obs.Sat, -1, nav);
					obs.satNav_ptr->geph_ptr 		= selgeph	(tsync, obs.Sat, -1, nav);
					obs.satNav_ptr->pephList_ptr	= &nav.pephMap[obs.Sat];
					obs.mount 						= id;
					updatenav(obs);

					obs.satStat_ptr					= &rec.rtk.satStatMap[obs.Sat];
					obs.satOrb_ptr					= &nav.orbpod.satOrbitMap[obs.Sat];
				}

				obsVariances(rec.obsList);

				//add this statino to the list of stations with data for this epoch
				epochStations.push_back(&rec);
			}
		}

		if	(tsync == GTime::noTime())
		{
			continue;
		}

		if	(epochStations.empty())
		{
			tsync.time += tgap;
			epoch++;
			continue;
		}

		// if the vmf3 option has been selected:
		if	(!acsConfig.tropOpts.vmf3dir.empty())
		{
			BOOST_LOG_TRIVIAL(debug)
			<< "VMF3 option chosen";

			for (auto& rec_ptr : epochStations)
			{
				auto& rec = *rec_ptr;

				double ep[6];
				time2epoch(rec.obsList.front().time, ep);
				double jd = ymdhms2jd(ep);

				// read vmf3 grid information
				rec.vmf3.m = 1;
				udgrid(acsConfig.tropOpts.vmf3dir.c_str(), rec.vmf3.vmf3g, jd, rec.mjd0, rec.vmf3.m);
			}
		}

		BOOST_LOG_TRIVIAL(info)
		<< "Synced " << epochStations.size() << " stations...";

		auto epochStartTime = boost::posix_time::from_time_t(system_clock::to_time_t(system_clock::now()));

#if ENABLE_UNIT_TESTS == 0
		Eigen::setNbThreads(1);
// #		pragma omp parallel for
#endif
		for (int i = 0; i < epochStations.size(); i++)
		{
			auto rec_ptr_iterator = epochStations.begin();
			std::advance(rec_ptr_iterator, i);

			Station& rec = **rec_ptr_iterator;

			mainOncePerEpochPerStation(tsync, rec, orog, gptg);
        }

		Eigen::setNbThreads(0);

		mainOncePerEpoch(tsync, ionKFState, netKFState, ionTrace, netTrace, epochStations, tgap);

		auto epochStopTime = boost::posix_time::from_time_t(system_clock::to_time_t(system_clock::now()));

		int week;
		double sec = time2gpst(tsync, &week);
		auto boostTime = boost::posix_time::from_time_t(tsync.time);

        BOOST_LOG_TRIVIAL(info)
		<< "Processed epoch #" << epoch
		<< " - " << "GPS time: " << week << " " << std::setw(6) << sec << " - " << boostTime << " (took " << (epochStopTime-epochStartTime) << ")";

        // Check end epoch
        if  ( !acsConfig.end_epoch.is_not_a_date_time()
			&&boostTime > acsConfig.end_epoch)
        {
            BOOST_LOG_TRIVIAL(info)
			<< "Exiting at epoch " << epoch << " (" << boostTime
			<< ") as end epoch " << acsConfig.end_epoch
			<< " has been reached";

            break;
        }

        // Check number of epochs
        if  ( acsConfig.max_epochs	> 0
			&&epoch					>= acsConfig.max_epochs)
        {
            BOOST_LOG_TRIVIAL(info)
			<< std::endl
			<< "Exiting at epoch " << epoch << " (" << boostTime
			<< ") as epoch count " << acsConfig.max_epochs
			<< " has been reached";

            break;
        }

		tsync.time += tgap;
        epoch++;
    }

	auto peaInterTime = boost::posix_time::from_time_t(system_clock::to_time_t(system_clock::now()));
	BOOST_LOG_TRIVIAL(info)
	<< std::endl
	<< "PEA started  processing at : " << peaStartTime << std::endl
	<< "and finished processing at : " << peaInterTime << std::endl
	<< "Total processing duration  : " << (peaInterTime - peaStartTime) << std::endl << std::endl;

    BOOST_LOG_TRIVIAL(info)
	<< std::endl
	<< "Finalising streams and post processing...";

 	mainPostProcessing(tsync, ionKFState, netKFState, ionTrace, netTrace, stationMap);

	auto peaStopTime = boost::posix_time::from_time_t(system_clock::to_time_t(system_clock::now()));

	BOOST_LOG_TRIVIAL(info)
	<< std::endl
	<< "PEA started  processing at : " << peaStartTime	<< std::endl
	<< "and finished processing at : " << peaStopTime	<< std::endl
	<< "Total processing duration  : " << (peaStopTime - peaStartTime) << std::endl << std::endl;

	BOOST_LOG_TRIVIAL(info)
	<< "PEA finished";

    return(EXIT_SUCCESS);
}
