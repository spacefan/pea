
#ifndef PRECEPH_HPP__
#define PRECEPH_HPP__

#include <list>

using std::list;

#include "acsStream.hpp"
#include "antenna.h"
#include "gaTime.hpp"
#include "satSys.hpp"

//forward declarations
struct nav_t;
struct Obs;
struct sta_t;

int readdcb(const char *file, nav_t *nav, list<sta_t*>& stations);
int peph2pos(
	Trace&		trace,
	GTime		time,
	SatSys&		Sat,
	nav_t&		nav,
	int			opt,
	Obs&		obs,
	PcoMapType*	pcoMap_ptr);


void 	readsp3(const char *file, nav_t *nav, int opt);
int  	readsap(const char *file, GTime time, nav_t *nav);
int  	readfcb(const char *file, nav_t *nav);
double	interppol(const double *x, double *y, int n);
int		orb2sp3(nav_t& nav);

int		pephclk(GTime time, string id, nav_t& nav, double *dts, double *varc);

#endif
