/*------------------------------------------------------------------------------
* readdcb.c : This file is used for reading and handling DCB/ISB files
*
*							CRCSI-GA ACS Project
*
* reference:
*			[1] SINEX_BIAS-Solution (Software/technique) INdependent EXchange
*               Format for GNSS BIASes Version 1.00
*-----------------------------------------------------------------------------*/

#include <boost/log/trivial.hpp>
#include "constants.h"
#include "navigation.hpp"

/* decodedcb decode satellite dcb-----------------------------------------------
 * args     :       char *buff      I     buffer
 *                  bias_t *sol     I/O	  current mgex info
 *                  int n           I 	  satellite dcb number
 *                  int id          I     1-station, 2-satellite
 *
 * return   :       0 not decode, 1 decode successfully
 * ---------------------------------------------------------------------------*/
int decodedcb(char *buff, bias_t *sol, int n, int id)
{
    bias_t sol0={0};
    char *p=NULL,*q=NULL;
    int i=0,m=0;

    if		(strstr(buff," DSB"))		sol0.type = 1;
	else if (strstr(buff," ISB"))		sol0.type = 0;
	else if (strstr(buff," OSB"))		sol0.type = 2;
	else
	{
		fprintf(stdout,"No bias type !!\n");
		return 0;
	}

    p=strtok(buff+34," ");
    while (p!=NULL) {
        i++;
        /* start time */
        if (i==1&&(q=strchr(p,':'))) {
            m=q-p;
            sol0.ts[0]=(int)strtol(p+m-2,NULL,10);
            if (sol0.ts[0]!=0)
                sol0.ts[0]+=2000;
            sol0.ts[1]=(int)strtol(q+1,NULL,10);
            sol0.ts[2]=(int)strtol(q+5,NULL,10);
        }
        /* end time */
        else if (i==2&&(q=strchr(p,':'))) {
            m=q-p;
            sol0.te[0]=(int)strtol(p+m-2,NULL,10);
            if (sol0.te[0]!=0)
                sol0.te[0]+=2000;
            sol0.te[1]=(int)strtol(q+1,NULL,10);
            sol0.te[2]=(int)strtol(q+5,NULL,10);
        }
        /* unit (ns) */
        else if (i==3) {

        }
        /* estimated value */
        else if (i==4) {
            sol0.val=strtod(p,NULL)*CLIGHT*1e-9;
        }
        /* std */
        else if (i==5) {
            sol0.std=strtod(p,NULL)*CLIGHT*1e-9;
        }
        p=strtok(NULL," ");
    }

#if (0)
    /* check start time validity */
    if (ctime[0]<sol0.ts[0]||
            (ctime[0]>=sol0.ts[0]&&ctime[1]<sol0.ts[1])||
            (ctime[0]>=sol0.ts[0]&&ctime[1]>=sol0.ts[1]&&
             ctime[2]<sol0.ts[2]))
        fprintf(stdout,"DCB file date is not valid!\n");
        return 0;

    /* check end time validity */
    if (ctime[0]>sol0.te[0]||
            (ctime[0]<=sol0.te[0]&&ctime[1]>sol0.te[1])||
            (ctime[0]<=sol0.te[0]&&ctime[1]<=sol0.te[1]&&
             ctime[2]>sol0.te[2]))
        fprintf(stdout,"DCB file date is not valid!\n");
        return 0;
#endif

    /* satellite */
    sol0.n=n+1;
    sol0.sys=buff[6];
    if (id==0) { /* for satellites */
        sol0.svn=(int)strtol(buff+7,NULL,10);
        sol0.prn=(int)strtol(buff+12,NULL,10);
    }
    else { /* for stations */
        strncpy(sol0.site,buff+15,4);
        strncpy(sol0.dome,buff+20,9);
    }

    sol0.code1.assign(buff+25,3);
	sol0.code2.assign(buff+30,3);

    /* avoid repeated end time and start time decoding */
    if (n != 0
		&&sol0.sys		== sol[n-1].sys
		&&sol0.prn		== sol[n-1].prn
		&&sol0.code1	== sol[n-1].code1
		&&sol0.code2	== sol[n-1].code2)
	{
        return 0;
	}

    sol[n] = sol0;

    return 1;
}
/* read SINEX_BIAS header block ------------------------------------------------
 * args     :       char *buff          I       buffer for lines
 *                  mgexbias_t *mbias   O       mgex bias
 * return   :       none
 * ---------------------------------------------------------------------------*/
void readheader(char *buff, mgexbias_t *mbias)
{
    char *p=NULL,*q=NULL;
    int i=0,n=0;
    /* read sinex_bias header */
    strncpy(mbias->id,buff+2,3);
    mbias->ver=strtod(buff+6,NULL);
    strncpy(mbias->agencycode,buff+11,3);

    /* to avoid year format conflicts */
    p=strtok(buff+14," ");
    while (p!=NULL) {
        if ((q=strchr(p,':'))) {
            i++;
            /* start time */
            if (i==2) {
                n=q-p;
                mbias->tbs[0]=(int)strtol(p+n-2,NULL,10);
                if (mbias->tbs[0]!=0)
                    mbias->tbs[0]+=2000;
                mbias->tbs[1]=(int)strtol(q+1,NULL,10);
                mbias->tbs[2]=(int)strtol(q+5,NULL,10);
            }
            /* end time */
            else if (i==3) {
                n=q-p;
                mbias->tbe[0]=(int)strtol(p+n-2,NULL,10);
                if (mbias->tbe[0]!=0)
                    mbias->tbe[0]+=2000;
                mbias->tbe[1]=(int)strtol(q+1,NULL,10);
                mbias->tbe[2]=(int)strtol(q+5,NULL,10);
            }
        }
        p=strtok(NULL," ");
    }

#if (0)
    mbias->nest=strtol(buff+60,NULL,10);
#endif

    return;
}
#if (0)
/* read SINEX_BIAS BIAS/DESCRIPTION block --------------------------------------
 * args     :       char *buff          I       buffer for lines
 *                  mgex_bias *mbias    O       mgex bias
 *                  int *ind            O       index for this block
 * return   :       none
 * ---------------------------------------------------------------------------*/
void readdescribe(char *buff, mgexbias_t *mbias, int *ind)
{
    if (*ind!=1) *ind=1;

    if (strstr(buff,"-BIAS/DESCRIPTION")) *ind=0;
    else if (strstr(buff,"DETERMINATION METHOD")||
             strstr(buff,"DETERMINATION_METHOD"))
        strncpy(mbias->method,buff+41,39);
    else if (strstr(buff,"BIAS MODE")||
             strstr(buff,"BIAS_MODE"))
        strncpy(mbias->mode,buff+41,39);
    else if (strstr(buff,"TIME SYSTEM")||
             strstr(buff,"TIME_SYSTEM"))
        strncpy(mbias->tsys,buff+41,3);
    else if (strstr(buff,"REFERENCE SYSTEM")||
             strstr(buff,"REFERENCE_SYSTEM"))
        strncpy(mbias->refsys,buff+41,1);
    else if (strstr(buff,"REFERENCE OBSERVABLES")||
             strstr(buff,"REFERENCE_OBSERVABLES")) {
        strncpy(mbias->satsys,buff+41,1);
        if (mbias->satsys=='G') {
            strncpy(mbias->gpscode[0],buff+44,4);
            strncpy(mbias->gpscode[1],buff+59,4);
        }
        else if (mbias->satsys=='R') {
            strncpy(mbias->glocode[0],buff+44,4);
            strncpy(mbias->glocode[1],buff+59,4);
        }
        else if (mbias->satsys=='C') {
            strncpy(mbias->bdscode[0],buff+44,4);
            strncpy(mbias->bdscode[1],buff+59,4);
        }
        else if (mbias->satsys=='E') {
            strncpy(mbias->galcode[0],buff+44,4);
            strncpy(mbias->galcode[1],buff+50,4);
        }
        else if (mbias->satsys=='J') {
            strncpy(mbias->qzscode[0],buff+44,4);
            strncpy(mbias->qzscode[1],buff+59,4);
        }
        else if (mbias->satsys=='I') {
            strncpy(mbias->irncode[0],buff+44,4);
            strncpy(mbias->irncode[1],buff+59,4);
        }
        else if (mbias->satsys=='S') {
            strncpy(mbias->sbacode[0],buff+44,4);
            strncpy(mbias->sbacode[1],buff+59,4);
        }
    }
}
#endif
/* read MGEX differential code bias --------------------------------------------
 * args     :       char *file	    	I       bias source file
 *                  mgex_bias *mbias	I/O     mgex bias
 * return   :       0-error, 1-ok
 * ---------------------------------------------------------------------------*/
extern int readdcb3(const char *file, mgexbias_t *mbias)
{
    FILE *fp;
    char buff[2048];
    int i=0,j=0,k=0,m=0,n=0,ind=0;
    int i1=0,j1=0,k1=0,m1=0,n1=0;

    if (!(fp=fopen(file,"r"))) {
        fprintf(stdout,"No %s \n",file);
        return 0;
    }

    /* read MGEX-DCB & IFB file */
    while (fgets(buff,sizeof(buff),fp))
	{
        if (buff[0]=='*') continue;

        /* read BIAS header line */
        if (strstr(buff,"%="))
		{
            if(strstr(buff,"BIA")&&!strstr(buff,"%=ENDBIA"))
			{
                readheader(buff,mbias);
            }
            else if (strstr(buff,"%=ENDBIA"))
			{
//                 fprintf(stdout,"End of MGEX-DCB\n");
                fclose(fp);
                return 1;
            }
            else
			{
//                 fprintf(stdout,"Not a SINEX BIAS file !!!\n");
                fclose(fp);
                return 0;
            }
        }

        /* read BIAS description block (omitted) */
        else if (strstr(buff,"+BIAS/DESCRIPTION")||ind==1)
		{
#if (0)
            readdescribe(buff,mbias,&ind);
#endif
        }

        /* differential signal (code) bias - DSB */
        else if (strstr(buff," DSB"))
		{
            if	( strstr(buff,"G   ")
				||strstr(buff,"R   ")
				||strstr(buff,"E   ")
				||strstr(buff,"C   ")
				||strstr(buff,"J   "))
			{
                /* station dcb */
                if (n < MAXDCBSTA - 1 && decodedcb(buff, mbias->dcbsta, n, 1))
                    n++;
            }
            else
			{
                /* satellite dcb */
                switch(buff[6])
				{
                    case 'G': if (i<MAXDCBGPS-1&&decodedcb(buff, mbias->dcbgps, i, 0)) i++; break;
                    case 'R': if (j<MAXDCBGLO-1&&decodedcb(buff, mbias->dcbglo, j, 0)) j++; break;
                    case 'E': if (k<MAXDCBGAL-1&&decodedcb(buff, mbias->dcbgal, k, 0)) k++; break;
                    case 'C': if (m<MAXDCBBDS-1&&decodedcb(buff, mbias->dcbbds, m, 0)) m++; break;
                }
            }
        }
        /* inter frequency bias */
        else if (strstr(buff," IFB"))
		{
            /* no data */
            if	( strstr(buff,"G   ")
				||strstr(buff,"R   ")
				||strstr(buff,"E   ")
				||strstr(buff,"C   ")
				||strstr(buff,"J   "))
			{
                /* station dcb */
                if (n1 < MAXDCBSTA - 1 && decodedcb(buff,mbias->ifbsta,n1,1))
                n1++;
            }
            else
			{
                /* satellite dcb */
                switch (buff[6])
				{
                    case 'G':  if (i1 < MAXDCBGPS - 1 && decodedcb(buff, mbias->ifbgps, i1, 0))     i1++; break;
                    case 'R':  if (j1 < MAXDCBGLO - 1 && decodedcb(buff, mbias->ifbglo, j1, 0))     j1++; break;
                    case 'E':  if (k1 < MAXDCBGAL - 1 && decodedcb(buff, mbias->ifbgal, k1, 0))     k1++; break;
                    case 'C':  if (m1 < MAXDCBBDS - 1 && decodedcb(buff, mbias->ifbbds, m1, 0))     m1++; break;
                }
            }
        }
    }

    fclose(fp);
    return 1;
}
/* search MGEX differential code bias ------------------------------------------
 * args     :       mgexbias_t *mbias	I     mgex bias
 *                  char *sta           I     site name (NULL if only for sat)
 *                  char sys            I     system
 *                  int prn             I     satellite prn
 *                  char *obs1          I     obs1 code
 *                  char *obs2          I     obs2 code
 *                  double dcb[2]       O     dcb value [0]-val,[2]-std
 *
 * return   :       0 not found, 1 found
 * ---------------------------------------------------------------------------*/
extern int searchdcb(
	mgexbias_t*	mbias,
	char*			sta,
	char			sys,
	int			prn,
	string		code1,
	string		code2,
	double				dcb[2])
{
    /* search station DCB */
    if (sta != NULL)
	{
        for (int i = 0; i < MAXDCBSTA; i++)
		{
            if	( sys	== mbias->dcbsta[i].sys
				&&!strncmp(sta,		mbias->dcbsta[i].site, 4)
				&&code1	== mbias->dcbgps[i].code1
				&&code2	== mbias->dcbgps[i].code2)
			{
                dcb[0] = mbias->dcbgps[i].val;
                dcb[1] = mbias->dcbgps[i].std;
                return 1;
            }
        }
        BOOST_LOG_TRIVIAL(debug) << "no DCB " << code1 << " and " << code2 << " for station " << sta;
        return 0;
    }
    /* search satellite DCB */
    switch(sys)
	{
        case 'G':
            for (int i = 0; i < MAXDCBGPS && prn <= NSATGPS; i++)
			{
				bias_t& bias = mbias->dcbgps[i];

				if	(prn != bias.prn)
				{
					continue;
				}

				if 	( code1	== bias.code1
					&&code2	== bias.code2)
				{
                    dcb[0] = +bias.val;
                    dcb[1] = +bias.std;
                    return 1;
                }

				if 	( code2	== bias.code1
					&&code1	== bias.code2)
				{
                    dcb[0] = -bias.val;
                    dcb[1] = +bias.std;
                    return 1;
                }
            }

//             fprintf(stdout,"GPS Sat=%3d DCB value= %lf, std= %lf \n", prn, dcb[0],dcb[1]);
            break;
        case 'R':
            for (int i = 0; i < MAXDCBGLO && prn <= NSATGLO; i++)
			{
				bias_t& bias = mbias->dcbglo[i];

				if	(prn != bias.prn)
				{
					continue;
				}

				if 	( code1	== bias.code1
					&&code2	== bias.code2)
				{
                    dcb[0] = +bias.val;
                    dcb[1] = +bias.std;
                    return 1;
                }

				if 	( code2	== bias.code1
					&&code1	== bias.code2)
				{
                    dcb[0] = -bias.val;
                    dcb[1] = +bias.std;
                    return 1;
                }
            }
#if (0)
//             fprintf(stdout,"GLO Sat=%3d DCB value= -------, std= ------- \n",
//                     prn,dcb[0],dcb[1]);
#endif
//             fprintf(stdout,"GLO Sat=%3d DCB value= %lf, std= %lf \n",
//                     prn,dcb[0],dcb[1]);
            break;
        case 'E':
            for (int i = 0; i< MAXDCBGAL && prn <= NSATGAL; i++)
			{
				bias_t& bias = mbias->dcbgal[i];

				if	(prn != bias.prn)
				{
					continue;
				}

				if 	( code1	== bias.code1
					&&code2	== bias.code2)
				{
                    dcb[0] = +bias.val;
                    dcb[1] = +bias.std;
                    return 1;
                }

				if 	( code2	== bias.code1
					&&code1	== bias.code2)
				{
                    dcb[0] = -bias.val;
                    dcb[1] = +bias.std;
                    return 1;
                }
            }
#if (0)
//             fprintf(stdout,"GAL Sat=%3d DCB value= -------, std= ------- \n",
//                     prn,dcb[0],dcb[1]);
#endif
//             fprintf(stdout,"GAL Sat=%3d DCB value= %lf, std= %lf \n",
//                     prn,dcb[0],dcb[1]);
            break;
        case 'C':
            for (int i = 0; i < MAXDCBBDS && prn <= NSATCMP; i++)
			{
				bias_t& bias = mbias->dcbbds[i];

				if	(prn != bias.prn)
				{
					continue;
				}

				if 	( code1	== bias.code1
					&&code2	== bias.code2)
				{
                    dcb[0] = +bias.val;
                    dcb[1] = +bias.std;
                    return 1;
                }

				if 	( code2	== bias.code1
					&&code1	== bias.code2)
				{
                    dcb[0] = -bias.val;
                    dcb[1] = +bias.std;
                    return 1;
                }
            }
#if (0)
//             fprintf(stdout,"BDS Sat=%3d DCB value= -------, std= ------- \n",
//                     prn,dcb[0],dcb[1]);
#endif
//             fprintf(stdout,"BDS Sat=%3d DCB value= %lf, std= %lf \n",
//                     prn,dcb[0],dcb[1]);
            break;
    }
    return 0;
}
