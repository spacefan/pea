
#ifndef ANTENNA_H
#define ANTENNA_H


#include <string>
#include <list>
#include <map>

using std::string;
using std::list;
using std::map;

#include "eigenIncluder.hpp"
#include <eigen3/unsupported/Eigen/CXX11/Tensor>

using Eigen::Tensor;


#include "streamTrace.hpp"
#include "constants.h"
#include "gaTime.hpp"
#include "enums.h"



typedef map<E_FType, Vector3d> PcoMapType;

struct pcvacs_t
{              /* antenna parameter type */
    int nf;                   /* number of frequencies */
    string type;            /* antenna type */
    string code;            /* serial number or satellite code */
    double aziDelta;              /* azimuth increment (degree) */
    double zenStart;
	double zenStop;
	double zenDelta;
    int nz;                   /* number of zenith intervals */
    int naz;                  /* number of non-azimuth dependent intervals */

    char sys[6];              /* frequency order by system */
    int ngps;                 /* GPS freq number (for rec pcv & pco) */
    int nglo;                 /* GLO freq number (for rec pcv & pco) */
    int ngal;                 /* GAL freq number (for rec pcv & pco) */
    int nbds;                 /* BDS freq number (for rec pcv & pco) */

    double tf[6];             /* valid from YMDHMS */
    double tu[6];             /* valid until YMDHMS */
    PcoMapType			pcoMap;			/* phase centre offsets (m) */
	MatrixXd			satPcvMat;		/* sat non-azimuth variations, elevation only */
	Tensor<double, 3>	recPcv3Mat;		/* rec azimuth/elevation variations */
};
typedef list<pcvacs_t> PcvList;


//forward declaration for pointer below
struct SatNav;
struct SatSys;

void satantoff(
	Trace&		trace,
	GTime		time,
	Vector3d&	rs,
	SatSys&		Sat,
	SatNav*		satNav_ptr,
	Vector3d&	dant,
	PcoMapType*	pcoMap_ptr);

void recpcv(pcvacs_t *pc, char sys, int freq, double el, double azi, double& pcv);
void recpco(pcvacs_t *pc, char sys, int freq, Vector3d& pco);
void satpcv(pcvacs_t *pc, double nadir, double *pcv);
pcvacs_t *findantenna(string type, string code, double tc[6], PcvList& pcvList, int id);
int readantexf(const char *file, PcvList& pcvList);
void radome2none(string& antenna_type);
void interp_satantmodel(pcvacs_t *pcv, double nadir, double *dant);

/* SINEX functions */
//forward declaration for pointers below
struct mgexbias_t;

int  readdcb3(const char *file, mgexbias_t *mbias);
int searchdcb(
	mgexbias_t*	mbias,
	char*		sta,
	char		sys,
	int			prn,
	string		code1,
	string		code2,
	double		dcb[2]);

#endif
