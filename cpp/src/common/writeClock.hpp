
#ifndef WRITECLK_HPP
#define WRITECLK_HPP


struct KFState;

void outputReceiverClocks(
	KFState&	kfState,
	double*		epoch,
	bool		smoothed = false);

void outputSatelliteClocks(
	KFState&	kfState,
	double*		epoch,
	bool		smoothed = false);

void outputClockfileHeader(
	KFState&	kfState,
	double*		epoch,
	bool		smoothed = false);


#endif
