
#ifndef ACS_CONFIG_H
#define ACS_CONFIG_H

#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/filesystem.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/program_options.hpp>

#include <yaml-cpp/yaml.h>

#include "eigenIncluder.hpp"

#include <unordered_map>
#include <memory>
#include <limits>
#include <vector>
#include <array>
#include <list>

using std::unordered_map;
using std::vector;
using std::string;
using std::array;
using std::list;

#include "acsStream.hpp"
#include "constants.h"
#include "satSys.hpp"
#include "enums.h"

/** Signal to noise ratio elevation masks
 */
struct snrmask_t
{        							/* SNR mask type */
	int		ena[2] = {};         	/* enable flag {rover,base} */
	double 	mask[NFREQ][9];			/* mask (dBHz) at 5,10,...85 deg */
};

/** Input source filenames and directories
 */
struct InputOptions
{
    vector<string>	atxfiles;
	vector<string>	snxfiles;
	vector<string>	blqfiles;

	vector<string>	navfiles;
	vector<string>	orbfiles;
	vector<string>	sp3files;
	vector<string>	clkfiles;

	vector<string>	erpfiles;
	vector<string>	dcbfiles;
	vector<string>	ionfiles;

	string	root_stations_dir	= "./";
	string	root_input_dir		= "./";
};

/** Enabling and setting destiations of program outputs.
 */
struct OutputOptions
{
	int		log_level					= 0;
	string	log_directory				= "./";
	string	log_filename				= "PEA<YYYY><DDD><HH>.log";
	double	log_rotate_period			= 0;

	int		trace_level					= 0;
	bool	output_trace				= false;
	string	trace_directory				= "./";
	string	trace_filename				= "<STATION><YYYY><DDD><HH>.trace";
	double	trace_rotate_period			= 0;

    bool	output_residuals 			= false;
	string	residuals_directory			= "./";
	string	residuals_filename			= "<STATION><DDD>.residuals";
	double	residuals_rotate_period		= 0;

    bool	output_summary 				= false;
	string	summary_directory			= "./";
	string	summary_filename			= "pea<YYYY><DDD><HH>.summary";
	double	summary_rotate_period		= 0;

    bool	output_clocks 				= false;
	string	clocks_directory			= "./";
	string	clocks_filename				= "pea<YYYY><DDD><HH>.clk";
	double	clocks_rotate_period		= 0;

    bool	output_ionex 				= false;
	string	ionex_directory				= "./";
	string	ionex_filename				= "pea<YYYY><DDD><HH>.ionex";
	string	iondsb_filename				= "pea<YYYY><DDD><HH>.iondcb";
	double	ionex_rotate_period			= 0;

    bool	output_iontrace				= false;
	string	iontrace_directory			= "./";
	string	iontrace_filename			= "pea<YYYY><DDD><HH>.IONTRACE";
	double	iontrace_rotate_period		= 0;

	double	config_rotate_period		= 0;

	bool	output_plots				= false;
	string	plot_directory				= "./";
	string	plot_filename				= "";

	bool	output_sinex				= false;
	string 	sinex_directory				= "./";
	string	sinex_filename				= "<CONFIG><WWWW><D>.snx";

	bool	output_persistance				= false;
	string 	persistance_directory			= "./";
	string	persistance_filename			= "<CONFIG><WWWW><D>.persist";
};

/** Options to be used only for debugging new features
 */
struct DebugOptions
{
	bool	debug_cs		= false;
	bool	debug_lom		= false;
	bool	debug_csacc		= false;

    int cscase = 0;         /* artificial CS case */
    int csfreq = 3;         /* cycle slip detection and repair frequency */
};

/** Options for unit testing
 */
struct TestOptions
{
	bool	stop_on_fail	= false;
	bool	stop_on_done	= false;
	bool	output_errors	= false;
	bool	absorb_errors	= false;
	string	directory		= "";
	string	filename		= "";
};

/** Options for the general operation of the software
 */
struct GlobalOptions
{
	double	epoch_interval	= 1;
	int		max_epochs		= 0;

    boost::posix_time::ptime start_epoch	{ boost::posix_time::not_a_date_time };
    boost::posix_time::ptime end_epoch		{ boost::posix_time::not_a_date_time };

	string	config_description			= "";
    string	analysis_agency				= "GAA";
    string	analysis_center				= "Geoscience Australia";
    string	analysis_program			= "AUSACS";
    string	rinex_comment				= "AUSNETWORK1";

	bool	process_user				= true;
	bool	process_network 			= false;
	bool	process_minimum_constraints	= false;
	bool	process_ionosphere			= false;
	bool	process_rts					= false;
	bool	process_tests				= false;

	bool	process_sys[E_Sys::NUM_SYS]	= {};

    double	elevation_mask	= 10 * D2R;

	string	pivot_station	= "<AUTO>";
	string	pivot_satellite	= "<AUTO>";

	bool	tide_solid		= false;
	bool	tide_otl		= false;
	bool	tide_pole		= false;

	bool	phase_windup	= true;
	bool	reject_eclipse	= true;
	bool	raim			= true;
	bool	antexacs		= true;
    bool 	clock_jump		= false;

	double	thres_slip   	= 0.05;
	double	max_inno     	= 30;
	double	max_gdop     	= 30;
	double	deweight_factor	= 100;

	double	wait_next_epoch		= 0.12;
	double	wait_all_stations	= 0;


	list<string>							station_files;
	std::multimap<string, ACSObsStreamPtr>	obsStreamMultimap;



	double clock_wrap_threshold = 0.05e-3;

	//to be removed?

    double proc_noise_iono	= 0.001;		//todo aaron, these need values, or move to other type


    int 	ppp_ephemeris      	= E_Ephemeris::PRECISE;
	bool 	satpcv             	= true;		//todo aaron add to yaml
    double	predefined_fail		= 0.001;	/* pre-defined fail-rate (0.01,0.001) */

	snrmask_t	snrmask;  			/* SNR mask */
};

/** Options associated with kalman filter states
 */
struct KalmanModel
{
	vector<double>	sigma				= {0};
	vector<double>	apriori_val			= {0};
	vector<double>	proc_noise			= {0};
	vector<double>	clamp_max			= {std::numeric_limits<double>::max()	};
	vector<double>	clamp_min			= {std::numeric_limits<double>::lowest()};
	bool			clamp				= false;
	int				proc_noise_model;
	bool			estimate 			= false;
};

/** Options associated with the network processing mode of operation
 */
struct NetworkOptions
{
	int			filter_mode		= E_FilterMode::KALMAN;
	int			inverter		= E_Inverter::INV;

	int			max_filter_iter = 2;
	int			max_prefit_remv = 2;

	int			rts_lag			= 0;
	string		rts_directory	= "./";
	string		rts_filename	= "Network-<YYYY><DDD><HH>.rts";

	KalmanModel		eop;
};

/** Options associated with the ionospheric modelling processing mode of operation
 */
struct IonosphericOptions
{
	int 		corr_mode  		= E_IonoMode::IONO_FREE_LINEAR_COMBO;
	int			iflc_freqs		= E_LinearCombo::ANY;
};

struct IonFilterOptions
{
	int				model			= E_IonoModel::SPHERICAL_CAPS;
	double			lat_center;
	double			lon_center;
	double			lat_width;
	double			lon_width;
	double			lat_res;
	double			lon_res;
	double			time_res;
	int				NpLayr;
	int				NBasis;
	int				func_order;
	double			model_noise;
	vector<double>	layer_heights;

	int				max_filter_iter = 2;
	int				max_prefit_remv = 2;
	int				inverter		= E_Inverter::INV;

	int		rts_lag			= 0;
	string	rts_directory	= "./";
	string	rts_filename	= "Ionosphere-<YYYY><DDD><HH>.rts";

	KalmanModel	ion;
};

/** Options to set the tropospheric model used in calculations
 */
struct TroposphericOptions
{
	int		model;
	string	vmf3dir;
	string	orography;
	string	gpt2grid;

	int 	corr_mode           	= 3;
};

/** Options to be applied to kalman filter states for individual satellites
 */
struct SatelliteOptions
{
	bool			_initialised	= false;
	bool			exclude			= false;

	KalmanModel		clk;
	KalmanModel		clk_rate;
	KalmanModel		orb;
};

/** Options to be applied to kalman filter states for individual receivers
 */
struct StationOptions
{
	bool			_initialised	= false;
	bool			exclude			= false;

	KalmanModel		amb;
	KalmanModel		pos;
	KalmanModel		pos_rate;
	KalmanModel		clk;
	KalmanModel		clk_rate;
	KalmanModel		dcb;
	KalmanModel		trop;
	KalmanModel		trop_grads;

	int				error_model	= E_NoiseModel::UNIFORM;
    vector<double>	code_sigmas	= {0.1};
    vector<double>	phas_sigmas	= {0.001};
};

/** Minimum constraint options for individual receivers
 */
struct MinimumStationOptions		//todo aaron, move to stations?
{
	bool			_initialised	= false;

	double			noise			= -1;
};

/** Options associated with the minimum constraints mode of operation
 */
struct MinimumConstraintOptions
{
	int		filter_mode				= E_FilterMode::LSQ;
	bool	estimate_scale			= false;
	bool	estimate_rotation		= false;
	bool	estimate_translation	= false;

	unordered_map<string,	MinimumStationOptions>		stationMap;
};

/** Optinos associated with the user mode (PPP) mode of operation
 */
struct PPPOptions
{
	int			outage_reset_count	= 50;		/* obs outage count to reset bias */
	int			phase_reject_count	= 10;		/* obs outage count to reset bias */

	int			max_filter_iter 	= 2;
	int			max_prefit_remv 	= 2;

	int			inverter			= E_Inverter::INV;

	int			rts_lag				= 0;
	string		rts_directory		= "./";
	string		rts_filename		= "PPP-<Station>-<YYYY><DDD><HH>.rts";

	bool		ballistics			= false;
};

/** General options object to be used throughout the software
 */
struct ACSConfig : GlobalOptions, InputOptions, OutputOptions, DebugOptions
{
    YAML::Node yaml;

    bool	parse(string filename, boost::program_options::variables_map& vm);
    void	info(Trace& trace);
	void	addStationFile(string fileName);

	SatelliteOptions&			getSatOpts		(SatSys&	Sat);
	StationOptions&				getRecOpts		(string		id);
	MinimumStationOptions&		getMinConOpts	(string 	id);

	unordered_map<string,		SatelliteOptions>	satOptsMap;
	unordered_map<string,		StationOptions>		recOptsMap;

	PPPOptions					pppOpts;
	TroposphericOptions			tropOpts;
	IonosphericOptions			ionoOpts;
	NetworkOptions				netwOpts;
	MinimumConstraintOptions	minCOpts;
	TestOptions					testOpts;

	IonFilterOptions			ionFilterOpts;

	KalmanModel	ion;
	list<string>	rinexFiles;
};






void replaceString(
	string&	str,
	string	subStr,
	string	replacement);

void removePath(
	string &filepath); 	// fully qualified file

void tryAddRootToPath(
	string& root,		///< Root path
	string& path);		///< Filename to prepend root path to

void replaceTimes(
	string&						str,		///< String to replace macros within
	boost::posix_time::ptime	time_time);	///< Time to use for replacements

bool configure(
	int		argc,
	char**	argv);

extern ACSConfig acsConfig;	///< Global variable housing all options to be used throughout the software



#endif
