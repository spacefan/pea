import dash
import dash_bootstrap_components as dbc

app = dash.Dash(__name__, suppress_callback_exceptions=True,external_stylesheets=[dbc.themes.BOOTSTRAP])

from data.get_data_sol import get_sol_data 
from data.get_data_res import get_res_data 
from data.helperfunc2 import get_sat_visible_to_rec

##Single SOURCE OF TRUTH data , to be used accross all sites
sol = get_sol_data()
res =  get_res_data()

sat_visible_to_rec  = get_sat_visible_to_rec()


server = app.server
