import pandas as pd

"""
Below functions creates a dataframe with columns x and y . It then sorts the dataframe based on values in Column x. 
The value in column x is generally the DateTime field.
"""

def sort_df(x,y):
    temp = pd.DataFrame({'x':x,'y':y})      
    sorted_df = temp.sort_values(by="x",ascending=True)
    
    return sorted_df
